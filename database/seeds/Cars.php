<?php

use Illuminate\Database\Seeder;

class Cars extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Car::create([
            'brand_id' => 2,
            'model_id' => 2,
            'year_of_car_manufacture' => 2020,
            'mileage' => 1,
            'color' => 'red',
            'price' => '1000.00',
        ]);

        \App\Models\Car::create([
            'brand_id' => 2,
            'model_id' => 3,
            'year_of_car_manufacture' => 2019,
            'mileage' => 222,
            'color' => 'yellow',
            'price' => '1001.00',
        ]);

        \App\Models\Car::create([
            'brand_id' => 2,
            'model_id' => 4,
            'year_of_car_manufacture' => 2015,
            'mileage' => 333,
            'color' => 'yellow',
            'price' => '10001.00',
        ]);

        \App\Models\Car::create([
            'brand_id' => 1,
            'model_id' => 1,
            'year_of_car_manufacture' => 2015,
            'mileage' => 123,
            'color' => 'gray',
            'price' => '70001.00',
        ]);
    }
}
