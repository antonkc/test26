<?php

use App\Models\CarBrand;
use Illuminate\Database\Seeder;

class CarBrands extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CarBrand::create([
            'name'              => 'Tesla',
        ]);

        CarBrand::create([
            'name'              => 'BMW',
        ]);

        CarBrand::create([
            'name'              => 'Ford',
        ]);

    }
}
