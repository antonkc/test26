<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Users::class);
        $this->call(CarBrands::class);
        $this->call(CarModels::class);
        $this->call(Cars::class);
    }
}
