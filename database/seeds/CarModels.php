<?php

use App\Models\CarModel;
use Illuminate\Database\Seeder;

class CarModels extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CarModel::create([
            'name'              => 'Model S',
            'brand_id'              => 1,
        ]);

        CarModel::create([
            'name'              => '325',
            'brand_id'              => 2,
        ]);
        CarModel::create([
            'name'              => '530',
            'brand_id'              => 2,
        ]);
        CarModel::create([
            'name'              => '740',
            'brand_id'              => 2,
        ]);

        CarModel::create([
            'name'              => 'Focus',
            'brand_id'              => 3,
        ]);
        CarModel::create([
            'name'              => 'Modeo',
            'brand_id'              => 3,
        ]);
        CarModel::create([
            'name'              => 'Mustang',
            'brand_id'              => 3,
        ]);

    }
}
