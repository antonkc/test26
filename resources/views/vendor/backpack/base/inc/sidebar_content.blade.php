<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('carbrand') }}'><i class='nav-icon la la-question'></i> {{__('Справочник марок')}}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('carmodel') }}'><i class='nav-icon la la-question'></i> {{__('Справочник моделей')}}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('car') }}'><i class='nav-icon la la-question'></i> {{__('Автомобили')}}</a></li>
