<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CarResource;
use App\Models\Car;
use Illuminate\Http\Request;

class CarsController extends APIBaseController
{
    public function index(Request $request)
    {
        $limit = (int)$request->get('limit');

        if ($limit != 0) {
            $cars = Car::take($limit)->get();
        } else {
            $cars = Car::all();
        }
        if (!$cars) {
            return $this->sendError([], __('The reviews was not found'));
        }

        return $this->sendResponse(CarResource::collection($cars), '');
    }
}
