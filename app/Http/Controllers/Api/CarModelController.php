<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CarModelResource;
use App\Models\CarModel;
use Illuminate\Http\Request;

class CarModelController extends APIBaseController
{
    public function index(Request $request)
    {
        $limit = (int)$request->get('limit');

        if ($limit != 0) {
            $carModels = CarModel::take($limit)->get();
        } else {
            $carModels = CarModel::all();
        }
        if (!$carModels) {
            return $this->sendError([], __('The reviews was not found'));
        }

        return $this->sendResponse(CarModelResource::collection($carModels), '');
    }
}
