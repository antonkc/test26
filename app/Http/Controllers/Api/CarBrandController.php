<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CarBrandResource;
use App\Models\CarBrand;
use Illuminate\Http\Request;

class CarBrandController extends APIBaseController
{
    public function index(Request $request)
    {
        $limit = (int)$request->get('limit');

        if ($limit != 0) {
            $carBrands = CarBrand::take($limit)->get();
        } else {
            $carBrands = CarBrand::all();
        }
        if (!$carBrands) {
            return $this->sendError([], __('The reviews was not found'));
        }

        return $this->sendResponse(CarBrandResource::collection($carBrands), '');
    }
}
