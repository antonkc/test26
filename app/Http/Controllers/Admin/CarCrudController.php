<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CarRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CarCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CarCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Car::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/car');
        CRUD::setEntityNameStrings('car', 'cars');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->setColumns([
            [
                'name'     => 'brand_id',
                'label'    => __('Car brand'),
                'type'     => 'closure',
                'function' => function ($entry) {
                    if (empty($entry->brand_id)) {
                        return __(' - ');
                    }
                    return \App\Models\CarBrand::find($entry->brand_id)->name;
                },
            ],

            [
                'name'     => 'model_id',
                'label'    => __('Car model'),
                'type'     => 'closure',
                'function' => function ($entry) {
                    if (empty($entry->model_id)) {
                        return __(' - ');
                    }
                    return \App\Models\CarModel::find($entry->model_id)->name;
                },
            ],

            [
                'name' => 'year_of_car_manufacture',
                'label' => __('Year of car manufacture'),
                'type' => 'text',
            ],
            [
                'name' => 'mileage',
                'label' => __('Mileage'),
                'type' => 'text',
            ],
            [
                'name' => 'color',
                'label' => __('Color'),
                'type' => 'text',
            ],
            [
                'name' => 'price',
                'label' => __('Price'),
                'type' => 'text',
            ],
        ]);

        // dropdown filter
        $this->crud->addFilter([
            'name'  => 'brand_id',
            'type'  => 'dropdown',
            'label' => 'Brand'
        ], [
            1 => 'Tesla',
            2 => 'BMW',
            3 => 'Ford',
        ], function($value) { // if the filter is active
            $this->crud->addClause('where', 'brand_id', $value);
        });


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CarRequest::class);

        $this->crud->AddFields([
            [  // Select
                'label' => __('Car brand'),
                'type' => 'select',
                'name' => 'brand_id', // the db column for the foreign key

                // optional
                // 'entity' should point to the method that defines the relationship in your Model
                // defining entity will make Backpack guess 'model' and 'attribute'
                'entity' => 'carBrands',

                // optional - manually specify the related model and attribute
                'model' => "App\Models\CarBrand", // related model
                'attribute' => 'name', // foreign key attribute that is shown to user
            ],
            [  // Select
                'label' => __('Car model'),
                'type' => 'select',
                'name' => 'model_id', // the db column for the foreign key

                // optional
                // 'entity' should point to the method that defines the relationship in your Model
                // defining entity will make Backpack guess 'model' and 'attribute'
                'entity' => 'carModels',

                // optional - manually specify the related model and attribute
                'model' => "App\Models\CarModel", // related model
                'attribute' => 'name', // foreign key attribute that is shown to user
            ],
            [
                'name' => 'year_of_car_manufacture',
                'label' => __('Year of car manufacture'),
                'type' => 'text',
            ],
            [
                'name' => 'mileage',
                'label' => __('Mileage'),
                'type' => 'text',
            ],
            [
                'name' => 'color',
                'label' => __('Color'),
                'type' => 'text',
            ],
            [
                'name' => 'price',
                'label' => __('Price'),
                'type' => 'text',
            ],
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
