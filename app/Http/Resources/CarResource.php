<?php

namespace App\Http\Resources;

use App\Models\CarBrand;
use App\Models\CarModel;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class CarResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'brand'                     => CarBrand::find($this->brand_id)->name,
            'model'                     => CarModel::find($this->model_id)->name,
            'year_of_car_manufacture'   => $this->year_of_car_manufacture,
            'mileage'                   => $this->mileage,
            'color'                     => $this->color,
            'price'                     => $this->price,
        ];
    }
}
