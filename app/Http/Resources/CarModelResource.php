<?php

namespace App\Http\Resources;

use App\Models\CarBrand;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CarModelResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'name'  => $this->name,
            'brand' => CarBrand::find($this->brand_id)->name,
        ];
    }
}
