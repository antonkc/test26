<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarModel extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use SoftDeletes;

    protected $table = 'car_models';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'brand_id'
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function carBrands()
    {
        return $this->belongsTo('App\Models\CarBrand', 'id', 'brand_id');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($carModel) {
            $car = new Car();
            $car->where('brand_id', $carModel->id)->delete();
        });
    }

}
