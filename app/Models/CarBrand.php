<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class CarBrand extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;

    protected $table = 'car_brands';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function carModels()
    {
        return $this->hasMany('App\Models\CarModel', 'id', 'brand_id');
    }

    public function cars()
    {
        return $this->hasMany('App\Models\Car', 'id', 'brand_id');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($carBrand) {
            $carModel = new CarModel();
            $car = new Car();

            $car->where('brand_id', $carBrand->id)->delete();
            $carModel->where('brand_id', $carBrand->id)->delete();
        });
    }
}
