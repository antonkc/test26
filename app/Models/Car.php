<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    protected $table = 'cars';
    protected $primaryKey = 'id';

    protected $fillable = [
        'brand_id',
        'model_id',
        'year_of_car_manufacture',
        'mileage',
        'color',
        'price',
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function carBrands()
    {
        return $this->belongsTo('App\Models\CarBrand', 'brand_id', 'id');
    }

    public function carModels()
    {
        return $this->belongsTo('App\Models\CarModel', 'model_id', 'id');
    }

}
